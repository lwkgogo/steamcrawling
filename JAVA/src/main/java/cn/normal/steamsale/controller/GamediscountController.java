package cn.normal.steamsale.controller;

import cn.normal.steamsale.utils.FanyiV3Demo;
import cn.normal.steamsale.entity.Gamediscount;
import cn.normal.steamsale.service.GamediscountService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

/**
 * (Gamediscount)表控制层
 *
 * @author makejava
 * @since 2021-02-22 14:03:04
 */
@RestController
public class GamediscountController {
    /**
     * 服务对象
     */
    @Resource
    private GamediscountService gamediscountService;

    @GetMapping("gameList")
    @CrossOrigin
    public List<List<String>> selectOne(Integer startNum,Integer endNum,String tagText) {
        QueryWrapper<Gamediscount> gamediscountQueryWrapper = new QueryWrapper<>();
        if (Objects.nonNull(tagText)&&tagText.length()>1){
            gamediscountQueryWrapper.like("tag",tagText);
        }
        gamediscountQueryWrapper.last(" limit "+(startNum>0?startNum:1)+","+endNum);
        List<Gamediscount> list = gamediscountService.list(gamediscountQueryWrapper);
        List<List<String>> lists = new ArrayList<>();
        ArrayList<String> games = new ArrayList<>();
        ArrayList<String> origins = new ArrayList<>();
        ArrayList<String> counts = new ArrayList<>();
        ArrayList<String> strens = new ArrayList<>();
        for (Gamediscount game:list) {
            String realName = game.getGname();
            if (FanyiV3Demo.isContainChinese(realName)) {
                games.add(realName);
            }else{
                if(!realName.contains("(")){
                    String FYName = FanyiV3Demo.toRequestForHttp(realName);
                    //存在中文则不进行翻译
                    if (FanyiV3Demo.isContainChinese(FYName)) {
                        realName = realName + "(" + FYName + ")";
                        game.setGname(realName);
                        //翻译后更新数据库 毕竟这接口翻译要收费的
                        gamediscountService.updateById(game);
                    }
                    games.add(realName);
                }
            }
            String strength = game.getDiscount().replaceAll("-","").replaceAll("%","");
            origins.add(game.getOriginalPrice()+"");
            counts.add(game.getFinalPrice()+"");
            strens.add(strength);
        }
        lists.add(games);
        lists.add(origins);
        lists.add(counts);
        lists.add(strens);
        return lists;
    }
    @RequestMapping("crawling")
    public Object doCrawlingSteam(){
        Configuration.headless = true;
        open("https://store.steampowered.com/specials#p=0&tab=TopSellers");
        String html = getWebDriver().getPageSource();
        Document doc = Jsoup.parse(html);
        Elements select = doc.select("#TopSellers_links");
        String[] pageNums = select.last().text().split(" ");
        int pageNum = Integer.parseInt(pageNums[pageNums.length - 1]);
        List<Gamediscount> games = new ArrayList<>();
        HashMap<String,String> gameMap = new HashMap<>();
        for (int j = 0;j<pageNum;j++){
            if (j<=4)
                $$("#TopSellers_links span").get(j).click();
            else if (j==pageNum-4) //倒数四页不做爬取处理
                break;
            else
                $$("#TopSellers_links span").get(4).click();
            sleep(1000);
            ElementsCollection elements = $$("#TopSellersRows > a");
            for (SelenideElement element : elements) {
                String info = element.text();
//                System.out.println(info);
                String[] s = info.replaceAll("¥ ","").replaceAll("\n"," ").split(" ");
                Gamediscount game = new Gamediscount();
                game.setDiscount(s[0]);
                game.setOriginalPrice(Double.parseDouble(s[1]));
                game.setFinalPrice(Double.parseDouble(s[2]));
                StringBuilder gameName = new StringBuilder();
                StringBuilder gameTag = new StringBuilder();
                int check = 0;
                for (int i = 3;i<s.length;i++) {
                    String nowStr = s[i];
                    if(nowStr.contains(",")){
                        check = i;
                        gameTag.append(nowStr);
                    }else if (check>0&&i>check){
                        gameTag.append(nowStr);
                    }else{
                        gameName.append(nowStr);
                    }
                }
                game.setGname(gameName.toString());
                game.setTag(gameTag.toString());
                game.setUsedate(new Date());
                //steam翻页可能存在重复
                if (gameMap.containsKey(gameName.toString())){
                    if(gameMap.get(gameName.toString()).equals(s[2])){
                        continue;
                    }
                }
                games.add(game);
                gameMap.put(gameName.toString(),s[2]);
            }
        }
        return "共"+pageNum+"页,预计爬取"+games.size()+"个优惠游戏,操作"+(gamediscountService.saveBatch(games)?"成功":"失败");
    }
}