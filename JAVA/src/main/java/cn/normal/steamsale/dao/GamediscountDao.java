package cn.normal.steamsale.dao;

import cn.normal.steamsale.entity.Gamediscount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * (Gamediscount)表数据库访问层
 *
 * @author makejava
 * @since 2021-02-22 14:03:02
 */
public interface GamediscountDao extends BaseMapper<Gamediscount>{

}