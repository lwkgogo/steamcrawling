package cn.normal.steamsale.service.impl;

import cn.normal.steamsale.entity.Gamediscount;
import cn.normal.steamsale.dao.GamediscountDao;
import cn.normal.steamsale.service.GamediscountService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * (Gamediscount)表服务实现类
 *
 * @author makejava
 * @since 2021-02-22 14:03:03
 */
 @Service
public class GamediscountServiceImpl extends ServiceImpl<GamediscountDao,Gamediscount> implements GamediscountService {
   
}