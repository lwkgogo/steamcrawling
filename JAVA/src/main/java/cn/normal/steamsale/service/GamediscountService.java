package cn.normal.steamsale.service;

import cn.normal.steamsale.entity.Gamediscount;
import com.baomidou.mybatisplus.extension.service.IService;
/**
 * (Gamediscount)表服务接口
 *
 * @author makejava
 * @since 2021-02-22 14:03:03
 */
public interface GamediscountService extends IService<Gamediscount>{

}