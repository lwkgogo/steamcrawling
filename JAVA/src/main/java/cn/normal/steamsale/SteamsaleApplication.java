package cn.normal.steamsale;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(value = "cn.normal.steamsale.dao")
public class SteamsaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SteamsaleApplication.class, args);
    }

}
