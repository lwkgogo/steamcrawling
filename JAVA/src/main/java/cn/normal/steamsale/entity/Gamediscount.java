package cn.normal.steamsale.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import java.io.Serializable;

/**
 * (Gamediscount)实体类
 *
 * @author makejava
 * @since 2021-02-22 14:03:02
 */
@Data
@TableName("Gamediscount")
public class Gamediscount implements Serializable {
    private static final long serialVersionUID = -54715567363048608L;
    @TableId(value = "gid",type = IdType.AUTO)
    private Integer gid;
    
    private String gname;

    private String discount;

    private Double originalPrice;

    private Double finalPrice;

    private String tag;

    private Date usedate;
    
    
}