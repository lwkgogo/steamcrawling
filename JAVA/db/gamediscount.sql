/*
Navicat MySQL Data Transfer

Source Server         : steam
Source Server Version : 50731
Source Host           : localhost:3306
Source Database       : steamsale

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2021-02-25 14:54:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gamediscount
-- ----------------------------
DROP TABLE IF EXISTS `gamediscount`;
CREATE TABLE `gamediscount` (
  `gid` int(11) NOT NULL AUTO_INCREMENT,
  `gname` varchar(200) DEFAULT NULL,
  `discount` varchar(10) DEFAULT NULL,
  `original_price` varchar(10) DEFAULT NULL,
  `final_price` double(10,0) DEFAULT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `useDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB AUTO_INCREMENT=886 DEFAULT CHARSET=utf8mb4;
