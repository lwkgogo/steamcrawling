package Worker

import (
	_ "database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
	"normal.cn/entity"
)

func getDB() (*sqlx.DB, error) {
	db, err := sqlx.Open("mysql", "root:130756@tcp(127.0.0.1:3306)/steamsale")
	if err != nil {
		log.Println(err)
		return nil, err
	}
	db.SetMaxOpenConns(30) //设置最大打开的连接数
	db.SetMaxIdleConns(1)  // 设置最大闲置连接数
	db.Ping()
	return db, nil
}

func insert(games []entity.Game) (b bool) {
	db, err := getDB()
	if db == nil{
		return false
	}
	db.Begin() // 开启事务
	stm, err := db.Prepare("insert into gamediscount(gname, discount, original_price, final_price,tag) values(?, ?, ?, ?, ?)")
	if err != nil {
		log.Println(err)
		return false
	}
	for i := range games {
		if len(games[i].Gname) != 0 {
			stm.Exec(games[i].Gname,games[i].Discount,games[i].Original_price,games[i].Final_price,games[i].Tag)
		}
	}
	stm.Close()
	defer db.Close()
	return true
}

func TestInsert() {
	games := []entity.Game{
		{Gname:"666",Discount:"666",Original_price:"20",Final_price:"5",Tag:"1515",UseDate:"2021-3-30"},
		{Gname:"777",Discount:"777",Original_price:"20",Final_price:"5",Tag:"1515",UseDate:"2021-3-30"},
		{Gname:"888",Discount:"888",Original_price:"20",Final_price:"5",Tag:"1515",UseDate:"2021-3-30"},
		{Gname:"999",Discount:"999",Original_price:"20",Final_price:"5",Tag:"1515",UseDate:"2021-3-30"},
	}
	fmt.Println(games)
	b := insert(games)
	if b {
		fmt.Println("插入成功")
	}
}