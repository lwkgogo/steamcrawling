package Worker

import (
	"fmt"
	"github.com/sclevine/agouti"
	"normal.cn/entity"
	"strconv"
	"strings"
	"sync"
	"time"
)
var Jobs = make(chan entity.GameJob, 5)
var results = make(chan entity.ResultSet, 5)

func crawling(wg *sync.WaitGroup){
	for job := range Jobs {
		GameInfo := GetGameInfo(job)
		if GameInfo==nil {
			continue
		}
		output := entity.ResultSet{Job: job, Results: GameInfo}
		results <- output
	}
	wg.Done()
}

func result(done chan bool) {
	for result := range results {
		gameInfo := result.Results
		games := make([]entity.Game, len(gameInfo),len(gameInfo))
		gameIndex := 0
		for s := range gameInfo  {
			if len(gameInfo[s])<1 {
				continue
			}
			gameStr := gameInfo[s]
			infos := strings.Split(gameStr,";")

			if len(infos[0])<1 {
				continue
			}
			tag := ""
			if len(infos)>=5 {
				tag = infos[4]
			}
			game := entity.Game{Gname:infos[3],Discount:infos[0],Original_price:infos[1],Final_price:infos[2],Tag:tag}
			games[gameIndex] = game
			gameIndex++
		}
		b := insert(games)
		if b {
			fmt.Println("第",result.Job.PageNum,"页,插入成功")
		}
	}
	done <- true
}

func createWorkerPool(noOfWorkers int) {
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go crawling(&wg)
	}
	wg.Wait()
	close(results)
}

func GetGameInfo(game entity.GameJob) []string{
	cd := agouti.ChromeDriver()
	// cd := agouti.PhantomJS()
	//cd := agouti.Selenium()
	err := cd.Start()
	if err != nil {
		fmt.Println("启动webDriver失败", err)
		return nil
	}
	defer cd.Stop()
	pg, err := cd.NewPage(agouti.Browser("chrome"))

	if err != nil {
		fmt.Println("启动新页面失败:", err)
		return nil
	}
	err = pg.SetPageLoad(20000)
	if err != nil {
		fmt.Println("页面加载等待失败:", err)
		return nil
	}
	err = pg.SetScriptTimeout(3000)
	if err != nil {
		fmt.Println("等待页面脚本超时:", game.PageNum)
		return nil
	}
	err = pg.Navigate(game.PageUri)
	_ = pg.SetPageLoad(3000)
	if err != nil {
		fmt.Println("打开页面失败,具体编号"+strconv.Itoa(game.PageNum))
		return nil
	}
	str2,err :=pg.FindByID("TopSellersRows").Text()
	if err != nil {
		fmt.Println("获取页面元素失败,具体编号" + strconv.Itoa(game.PageNum))
		return nil
	}
	str2 = strings.ReplaceAll(str2,"¥","")
	str2 = strings.ReplaceAll(str2,"\n",";")
	str2 = strings.ReplaceAll(str2,"\t",";")
	strs := strings.Split(str2, "-")
	sli := make([]string,len(strs),len(strs))
	sliceIndex := 0
	for s := 1;s<=len(strs)-1;s++  {
		if checkStrTrue([]rune(strs[s])){
			sli[sliceIndex] = strs[s]
		}else {
			if s == len(strs)-1{
				sli[sliceIndex] = strs[s]
			}else {
				if checkStrTrue([]rune(strs[s] + strs[s+1])) {
					sli[sliceIndex] = strs[s]+strs[s+1]
					s++
				} else {
					s = s + 2
				}
			}
		}
		sliceIndex++
	}
	return sli
}

func checkStrTrue(check []rune) bool{
	checkNum := 0
	for c := range check  {
		if check[c]==';'{
			checkNum = checkNum + 1
		}
	}
	if checkNum >= 5 {
		return true
	}
	return false
}

func createJobList(countNum int){
	for i := 0; i<countNum ; i++ {
		job := entity.GameJob{PageNum:i,PageUri:"https://store.steampowered.com/specials#p="+strconv.Itoa(i)+"&tab=TopSellers"}
		Jobs <- job
	}
	close(Jobs)
}

func getAllPages() int{
	//ScrapeHtml()
	cd := agouti.ChromeDriver()
	// cd := agouti.PhantomJS()
	//cd := agouti.Selenium()
	err := cd.Start()
	if err != nil {
		fmt.Println("Start webDriver:", err)
		return 0
	}
	defer cd.Stop()
	pg, err := cd.NewPage(agouti.Browser("chrome"))

	if err != nil {
		fmt.Println("NewPage:", err)
		return 0
	}
	err = pg.SetPageLoad(10000)
	if err != nil {
		fmt.Println("SetPageLoad:", err)
		return 0
	}
	err = pg.SetScriptTimeout(1000)
	if err != nil {
		fmt.Println("SetScriptTimeout:", err)
		return 0
	}
	err = pg.Navigate("https://store.steampowered.com/specials#p=0&tab=TopSellers")
	if err != nil {
		fmt.Println("Navigate:", err)
		return 0
	}
	str,_ := pg.FindByID("TopSellers_links").Text()
	str = string([]rune(str)[len(str)-2:len(str)])
	nums, err := strconv.Atoi(str)
	return nums
}

func MainWork(){
	startTime := time.Now()
	nums := getAllPages()
	if nums == 0 {
		fmt.Println("出错了 等会再来吧")
	}else {
		go createJobList(nums)
		done := make(chan bool)
		go result(done)
		noOfWorkers := 5
		createWorkerPool(noOfWorkers)
		<-done
		endTime := time.Now()
		diff := endTime.Sub(startTime)
		fmt.Println("总耗时", diff.Seconds(), "秒")
	}
}