package entity

type GameJob struct {
	PageNum int
	PageUri string
}
type ResultSet struct {
	Job GameJob
	Results []string
}
type Game struct {
	Gid   int    `db:"gid"`
	Gname string `db:"gname"`
	Discount      string `db:"discount"`
	Original_price    string `db:"original_price"`
	Final_price    string `db:"final_price"`
	Tag    string `db:"tag"`
	UseDate    string `db:"useDate"`
}
